# HUGO GENERATE (wm/website make)
a command line shell script helper for hugo

```
HUGO GENERATE (wm/website make) - A shell script helper for hugo

wm [options]

options:
-h, --help                              show brief help
-d, --directory     <path>|<blank>      use a custom hugo directory
-w, --web-directory <path>|<blank>      use a custom web directory
                                        (relative to content/ directory)

-n, --new  <path>|<blank>               make new page and edit
                                        (in default directory)
-N         <name>|<blank>               make new page and edit
                                        (relative to content/ directory)
-e, --edit <name>|<blank>               edit file in default directory
-E         <path>|<blank>               edit file (relative to content/
                                        directory)

-v, --view                              view content directory
-s, --server, --serve                   run hugo server at localhost
-ss, --server-terminal                  run hugo server at localhost
                                        (term)
-S, --settings                          view defaults
-u, --upload                            upload to remote server (custom)
-k. --kill                              stop all running hugo processes


NOTE: default directories are defined at the top of the script
```

Installation: (in terminal)

```
# add $HOME/.local/bin to path
echo "export PATH=$PATH:$HOME/.local/bin" >> ~/.bashrc
# clone the repository
git clone https://codeberg.org/dvr/wm
# install
cd wm
cp wm $HOME/.local/bin/
mkdir -p $HOME/.config/hugo-shell-script/wm/defaults
cp defaults $HOME/.config/hugo-shell-script/wm/defaults
```

made in August 24 2022
